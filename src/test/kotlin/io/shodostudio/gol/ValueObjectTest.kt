package io.shodostudio.gol

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class ValueObjectTest {

    @ParameterizedTest
    @MethodSource("equality")
    fun `should assert equality`(left: Any, right: Any) {
        assertThat(left).isEqualTo(right)
        assertThat(left.hashCode()).isEqualTo(right.hashCode())
    }

    @Test
    fun `should assert inequality`() {
        assertThat(Grid(20, 30)).isNotEqualTo(Grid(10, 30))
        assertThat(Grid(20, 30).hashCode()).isNotEqualTo(Grid(10, 30).hashCode())
    }

    companion object {
        @JvmStatic
        fun equality(): Stream<Arguments> {
            return Stream.of(Arguments.of(Grid(10, 30), Grid(10, 30)))
        }
    }
}


