package io.shodostudio.gol

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class GridTest {

    @Test
    fun `should list all neighbours`() {
        val grid = Grid(width = 10, height = 10)

        assertThat(
            grid.neigboursOf(Position(5, 5)))
        .containsExactlyInAnyOrder(
            Position(5 - 1, 5 - 1),
            Position(5 - 1, 5),
            Position(5 - 1, 5 + 1),
            Position(5, 5 - 1),
            Position(5, 5 + 1),
            Position(5 + 1, 5 - 1),
            Position(5 + 1, 5),
            Position(5 + 1, 5 + 1)
        )
    }

    @Test
    fun `should list all neighbours for top left corner`() {
        val (grid, items) = """
        ┌──────┐
        │◯A    │
        │CB    │
        │      │
        └──────┘
        """.grid()

        assertThat(
            grid.neigboursOf(items["◯"]))
        .containsExactlyInAnyOrder(
            items["A"],
            items["B"],
            items["C"],
        )
    }

    @Test
    fun `should list all neighbours for top right corner`() {
        val (grid, items) = """
        ┌──────┐
        │    A◯│
        │    CB│
        │      │
        └──────┘
        """.grid()

        assertThat(
            grid.neigboursOf(items["◯"]))
            .containsExactlyInAnyOrder(
                items["A"],
                items["B"],
                items["C"],
            )
    }

    @Test
    fun `should list all neighbours for bottom left corner`() {
        val (grid, items) = """
        ┌──────┐
        │      │
        │AB    │
        │◯C    │
        └──────┘
        """.grid()

        assertThat(
            grid.neigboursOf(items["◯"]))
            .containsExactlyInAnyOrder(
                items["A"],
                items["B"],
                items["C"],
            )
    }


    @Test
    fun `should list all neighbours for bottom right corner`() {
        val (grid, items) = """
        ┌──────┐
        │      │
        │    AB│
        │    C◯│
        └──────┘
        """.grid()

        assertThat(
            grid.neigboursOf(items["◯"]))
            .containsExactlyInAnyOrder(
                items["A"],
                items["B"],
                items["C"],
            )
    }

    @Test
    fun `should list all neighbours for bottom`() {
        val (grid, items) = """
        ┌──────┐
        │      │
        │ ABC  │
        │ E◯D  │
        └──────┘
        """.grid()

        assertThat(
            grid.neigboursOf(items["◯"]))
            .containsExactlyInAnyOrder(
                *items.filter { it.symbol != "◯" }.map { it.position }.toTypedArray()
            )
    }

    @Test
    fun `should list all neighbours for top`() {
        val (grid, items) = """
        ┌──────┐
        │ E◯D  │
        │ ABC  │
        │      │
        └──────┘
        """.grid()

        assertThat(
            grid.neigboursOf(items["◯"]))
            .containsExactlyInAnyOrder(
                *items.filter { it.symbol != "◯" }.map { it.position }.toTypedArray()
            )
    }

    @Test
    fun `should list all neighbours for left`() {
        val (grid, items) = """
        ┌──────┐
        │AB    │
        │◯C    │
        │ED    │
        └──────┘
        """.grid()

        assertThat(
            grid.neigboursOf(items["◯"]))
            .containsExactlyInAnyOrder(
                *items.filter { it.symbol != "◯" }.map { it.position }.toTypedArray()
            )
    }

    @Test
    fun `should list all neighbours for right`() {
        val (grid, items) = """
        ┌──────┐
        │    BA│
        │    C◯│
        │    DE│
        └──────┘
        """.grid()

        assertThat(
            grid.neigboursOf(items["◯"]))
            .containsExactlyInAnyOrder(
                *items.filter { it.symbol != "◯" }.map { it.position }.toTypedArray()
            )
    }

    @Test
    fun `size should be browsable`() {
        val positions: Set<Position> =
        Grid(3, 4).traverse {
                position -> position
        }.toSet()

        assertThat(positions.size).isEqualTo(3 * 4)
    }
}