package io.shodostudio.gol

import org.assertj.core.api.Assertions.assertThat
import java.util.*
import kotlin.test.Test

class GoLTest {

    @Test
    fun `should create an empty grid`() {
        val width = Random().nextInt(100)
        val height = Random().nextInt(100)
        val board = Board(of=Grid(width, height), emptyList())

        assertThat(board.`alive cells`).isEmpty()
    }

    @Test
    fun `Any live cell with fewer than two live neighbours dies, as if by underpopulation`() {
        """
        ┌──────┐ ┌──────┐ ┌──────┐
        │ ◯    │ │      │ │      │
        │  ◯   │ │  ◯   │ │      │
        │   ◯  │ │      │ │      │
        └──────┘ └──────┘ └──────┘
        """.play()
    }

    @Test
    fun `Any live cell with two or three live neighbours lives on to the next generation`() {
        """
        ┌──────┐ ┌──────┐
        │  ◯   │ │  ◯   │
        │ ◯ ◯  │ │ ◯ ◯  │
        │  ◯◯  │ │  ◯◯◯ │
        │    ◯ │ │   ◯  │
        └──────┘ └──────┘
        """.play()
    }

    @Test
    fun `Any live cell with more than three live neighbours dies, as if by overpopulation`() {
        """
        ┌──────┐ ┌──────┐
        │  ◯   │ │ ◯◯◯  │
        │ ◯◯◯  │ │ ◯ ◯  │
        │  ◯   │ │ ◯◯◯  │
        └──────┘ └──────┘
        """.play()
    }

    @Test
    fun `Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction`() {
        """
        ┌──────┐ ┌──────┐
        │  ◯   │ │ ◯◯   │
        │ ◯◯   │ │ ◯◯   │
        │      │ │      │
        └──────┘ └──────┘
        """.play()
    }

    @Test
    fun `Repeating pattern`() {
        """
        ┌──────┐ ┌──────┐ ┌──────┐ ┌──────┐
        │      │ │  ◯   │ │      │ │  ◯   │
        │ ◯◯◯  │ │  ◯   │ │ ◯◯◯  │ │  ◯   │
        │      │ │  ◯   │ │      │ │  ◯   │
        └──────┘ └──────┘ └──────┘ └──────┘
        """.play()
    }
}


data class Item(val symbol: String, val position: Position)
data class GridDescription(val grid: Grid, val items: List<Item>)


operator fun List<Item>.get(c: String): Position {
    return this.first { it.symbol == c }.position
}

fun String.play() {
    val boards = this.boards()

    val board = boards.take(1).first()

    boards.drop(1).forEach { expected ->
        board.play()
        assertThat(board).isEqualTo(expected)
    }
}

fun List<String>.grid(): GridDescription {
    val cases = this.drop(1).dropLast(1).map {
        val start = it.indexOf("│")
        val end = it.indexOf("│", start + 1)
        it.substring(start + 1, end)
    }

    val width = cases.first().count()
    val height = cases.count()

    val items = cases.mapIndexed { y, s -> s.mapIndexed { x, c -> Item("$c", Position(x, y)) } }.flatten().filter { it.symbol != " " }

    return GridDescription(Grid(width, height), items)
}

fun String.grid(): GridDescription = trimIndent().split("\n").dropWhile { !it.contains('┌') }.grid()

fun GridDescription.board(): Board {
    return Board(grid, items.map { Cell(it.position, Is.Alive) })

}

fun String.boards(): List<Board> {
    val lines = trimIndent().split("\n").dropWhile { !it.contains('┌') }

    var columnFirstActual = lines.first().indexOf('┌')
    val grids = mutableListOf<List<String>>()
    while( columnFirstActual != -1) {
        val columnLastActual = lines.first().indexOf('┐', columnFirstActual)
        grids.add(
            lines.map { line -> line.subSequence(columnFirstActual, columnLastActual + 1).toString() }.reversed()
        )

        columnFirstActual = lines.first().indexOf('┌', columnFirstActual + 1)
    }

    return grids.map { it.grid().board() }
}

