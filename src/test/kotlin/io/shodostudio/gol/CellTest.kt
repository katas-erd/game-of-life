package io.shodostudio.gol

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class CellTest {

    @Test
    fun `should be identified by its position`() {
        assertThat(Cell(at(2, 5), Is.Alive)).isEqualTo(Cell(at(2, 5), Is.Dead))
        assertThat(Cell(at(2, 5), Is.Alive).hashCode()).isEqualTo(Cell(at(2, 5), Is.Dead).hashCode())

        assertThat(Cell(at(2, 5), Is.Alive)).isNotEqualTo(Cell(at(6, 15), Is.Alive))
        assertThat(Cell(at(2, 5), Is.Alive).hashCode()).isNotEqualTo(Cell(at(6, 15), Is.Alive).hashCode())
    }

    @Test
    fun `an alive cell with two live neighbours should stay alive`() {
        assertThat(
            Cell(at(0, 0), Is.Alive).evolved(
            listOf(
                Cell(at(0, 1), Is.Alive),
                Cell(at(1, 0), Is.Alive)
            )
        )).isEqualTo(Cell(at(0, 0), Is.Alive))
    }

    @Test
    fun `an alive cell with fewer than two live neighbours should die`() {
        assertThat(
            Cell(at(0, 0), Is.Alive).evolved(
                listOf(
                    Cell(at(0, 1), Is.Dead),
                    Cell(at(1, 0), Is.Dead)
                )
            )).isEqualTo(Cell(at(0, 0), Is.Dead))
    }

    @Test
    fun `A live cell with two or three live neighbours should live`() {
        assertThat(
            Cell(at(0, 0), Is.Dead).evolved(
                listOf(
                    Cell(at(0, 1), Is.Alive),
                    Cell(at(1, 0), Is.Alive)
                )
            )).isEqualTo(Cell(at(0, 0), Is.Alive))
        assertThat(
            Cell(at(0, 0), Is.Dead).evolved(
                listOf(
                    Cell(at(0, 1), Is.Alive),
                    Cell(at(1, 0), Is.Alive),
                    Cell(at(-1, 0), Is.Alive)
                )
            )).isEqualTo(Cell(at(0, 0), Is.Alive))
    }

    @Test
    fun `A live cell with more than three live neighbours should die`() {
        assertThat(
            Cell(at(0, 0), Is.Dead).evolved(
                listOf(
                    Cell(at(0, 1), Is.Alive),
                    Cell(at(1, 0), Is.Alive)
                )
            )).isEqualTo(Cell(at(0, 0), Is.Alive))
        assertThat(
            Cell(at(0, 0), Is.Dead).evolved(
                listOf(
                    Cell(at(0, 1), Is.Alive),
                    Cell(at(1, 0), Is.Alive),
                    Cell(at(-1, 0), Is.Alive),
                    Cell(at(1, -1), Is.Alive)
                )
            )).isEqualTo(Cell(at(0, 0), Is.Dead))
    }
}


