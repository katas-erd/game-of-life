package io.shodostudio.gol

import org.junit.jupiter.api.Test

class BoardTest {
    @Test
    fun `should give the neighbours of a cell`() {
        `in the board`("""
          012345
         ┌──────┐
        0│      │
        1│  ◯   │
        2│      │
         └──────┘    
        """).calling {
            `neighbours of`(Position(2,1))
        }
    }

    private fun `neighbours of`(position: Position) {

    }
}


private fun `in the board`(description: String): BoardTestHelper {
    return BoardTestHelper(description.trimIndent().boards().first())
}
class BoardTestHelper(val board: Board) {
    fun <T> calling(f: Board.() -> T): T {
        return board.f()
    }
}