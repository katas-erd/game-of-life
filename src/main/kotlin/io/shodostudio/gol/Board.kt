package io.shodostudio.gol

import java.util.*

class Board(of: Grid, initialCells: List<Cell>) {
    private var cells = initialCells.filter { it.state == Is.Alive }.associateBy { it.position }

    fun play() {
        cells = size.traverse {
            position -> applyRulesForCellAt(position)
        }.filter {
            it.state == Is.Alive
        }.associateBy { it.position }
    }


    private fun applyRulesForCellAt(position: Position): Cell {
        val neighbours = size.neigboursOf(position).map { p -> get(p) }
        val cell = get(position)
        return cell.evolved(neighbours)
    }

    private fun get(position: Position) = cells.getOrDefault(position, Cell(position, Is.Dead))

    val `alive cells`: List<Cell>
        get() = cells.values.toList()

    private val size = of

    override fun equals(other: Any?): Boolean {
        return other is Board && size == other.size && `alive cells`.containsAll(other.`alive cells`) && other.`alive cells`.containsAll(`alive cells`)
    }

    override fun toString(): String {
        val (w, h) = size
        val s = Collections.nCopies(h, ".".repeat(w)).toMutableList()
        size.traverse {
                position ->
            if (get(position).state == Is.Alive) {
                s[position.y] = s[position.y].take(position.x) + "X" + s[position.y].takeLast(w - position.x -1)
            }
        }.toList()

        return s.reversed().joinToString("\n")
    }
}