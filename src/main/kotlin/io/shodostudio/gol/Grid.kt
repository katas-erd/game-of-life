package io.shodostudio.gol

data class Grid(val width: Int, val height: Int) {
    fun <T> traverse(f: (Position) -> T) = sequence {
        for(x in 0..width - 1) {
            for (y in 0.. height - 1) {
                yield(f(at(x, y)))
            }
        }
    }

    fun neigboursOf(position: Position): List<Position> {
        return listOf(
            Delta(-1, -1),
            Delta(-1,  0),
            Delta(-1, +1),
            Delta( 0, -1),
            Delta( 0, +1),
            Delta(+1, -1),
            Delta(+1,  0),
            Delta(+1, +1)
        )
            .map { position + it }
            .filter { it.isInGrid()  }
    }

    private fun Position.isInGrid(): Boolean {
        val (x, y) = this
        return x in (0..width - 1) && y in 0.. height - 1
    }
}