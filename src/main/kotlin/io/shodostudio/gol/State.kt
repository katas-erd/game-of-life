package io.shodostudio.gol

enum class Is {
    Alive,
    Dead
}