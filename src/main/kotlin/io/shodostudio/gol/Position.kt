package io.shodostudio.gol

data class Delta(val dx: Int, val dy: Int)
data class Position(val x: Int, val y: Int) {
    operator fun plus(delta: Delta) = Position(x + delta.dx, y + delta.dy)
}

fun at(x: Int, y: Int) = Position(x, y)