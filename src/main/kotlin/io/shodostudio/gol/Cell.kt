package io.shodostudio.gol

data class Cell(val position: Position, val state: Is) {
    override fun equals(other: Any?): Boolean {
        return other is Cell && position == other.position
    }

    override fun hashCode(): Int {
        return position.hashCode()
    }

    fun evolved(neighbours: List<Cell>): Cell {
        val aliveNeighbours = neighbours.count {it.state == Is.Alive}

        return when (state) {
            Is.Alive -> evolvedWhenAlive(aliveNeighbours)
            Is.Dead -> evolvedWhenDead(aliveNeighbours)
        }
    }

    private fun evolvedWhenAlive(aliveNeighbours: Int): Cell {
        if (aliveNeighbours < 2 || aliveNeighbours > 3)
            return Cell(position, Is.Dead)
        return this
    }

    private fun evolvedWhenDead(aliveNeighbours: Int): Cell {
        if (aliveNeighbours == 3)
            return Cell(position, Is.Alive)
        return this
    }
}